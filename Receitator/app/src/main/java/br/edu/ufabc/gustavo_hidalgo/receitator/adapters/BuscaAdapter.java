package br.edu.ufabc.gustavo_hidalgo.receitator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;

/**
 * Created by hdant on 09/04/2017.
 */

public class BuscaAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<String> mListaNomeIng = null;
    private ArrayList<String> arraylist;

    public BuscaAdapter(Context context, ArrayList<String> listaNomeIng) {
        mContext = context;
        this.mListaNomeIng = listaNomeIng;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(listaNomeIng);
    }

    public class ViewHolder {
        TextView name;

    }

    @Override
    public int getCount() {
        return mListaNomeIng.size();
    }

    @Override
    public String getItem(int position) {
        return mListaNomeIng.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.busca_adapter, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(mListaNomeIng.get(position));
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mListaNomeIng.clear();
        if (charText.length() == 0) {
            mListaNomeIng.addAll(arraylist);
        } else {
            for (String wp : arraylist) {
                if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
                    mListaNomeIng.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
