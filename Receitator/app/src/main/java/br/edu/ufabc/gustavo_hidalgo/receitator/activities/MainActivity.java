package br.edu.ufabc.gustavo_hidalgo.receitator.activities;

import android.Manifest;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.fragments.ReceitaCalculadaFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.fragments.ReceitaIngFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.fragments.ReceitaNomeFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.fragments.ReceitaProntaFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.fragments.InfoReceitaFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.fragments.TelaInicialFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IMainActivity;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;
import br.edu.ufabc.gustavo_hidalgo.receitator.utils.FileUtils;

public class MainActivity extends AppCompatActivity implements IMainActivity {
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    public Fragment mFragment;
    public FragmentManager mFragmentManager;
    public FragmentTransaction mFragmentTransaction;
    static Receita mReceita;
    static JSONArray mJsonArray;
    static ArrayList<String> mReceitasSalvas;
    static File mArquivoReceitasSalvas;
    public static final String RECEITAS_SALVAS = "receitas_salvas.json";
    public static final String TAG = "[Rectt]MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getFragmentManager();

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        mArquivoReceitasSalvas = new File(Environment.getExternalStorageDirectory() +
                File.separator + RECEITAS_SALVAS);
        mReceitasSalvas = new ArrayList<>();

        if(mArquivoReceitasSalvas.exists()) {
            try {
                JSONObject jo = new JSONObject(FileUtils
                        .loadJSONFromFile(this, mArquivoReceitasSalvas.getPath(), false));
                mJsonArray = jo.getJSONArray("Receitas");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < mJsonArray.length(); i++) {
                try {
                    JSONObject jo = mJsonArray.getJSONObject(i);
                    mReceitasSalvas.add(jo.getString("mNome"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            mReceitasSalvas.add("Sem receitas");
        }

        changeFragment(Constantes.TELA_INICIAL, mJsonArray, mReceitasSalvas);
    }

    @Override
    public void changeFragment(int fragment, JSONArray jsonArray, ArrayList<String> arrayList) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        Log.d(TAG,String.valueOf(fragment)+" - changing frag");
        switch (fragment) {
            case Constantes.TELA_INICIAL:
                mFragment = new TelaInicialFragment();
                break;
        }
        mFragmentTransaction.replace(R.id.frame, mFragment);
        mFragmentTransaction.commit();
    }

    @Override
    public void changeFragment(int fragment, Receita receita){
        mFragmentTransaction = mFragmentManager.beginTransaction();
        Log.d(TAG,String.valueOf(fragment)+" - changing frag");
        switch (fragment){
            case Constantes.RECEITA_NOME:
                mReceita = new Receita();
                mFragment = ReceitaNomeFragment.newInstance(mReceita);
                break;
            case Constantes.RECEITA_ING:
                mReceita = receita;
                mFragment = ReceitaIngFragment.newInstance(mReceita);
                break;
            case Constantes.RECEITA_PRONTA:
                mReceita = receita;
                mFragment = ReceitaProntaFragment.newInstance(mReceita);
                break;
            case Constantes.RECEITA_CALCULADA:
                mReceita = receita;
                mFragment = ReceitaCalculadaFragment.newInstance(mReceita);
                break;
        }
        mFragmentTransaction.replace(R.id.frame, mFragment);
        mFragmentTransaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
