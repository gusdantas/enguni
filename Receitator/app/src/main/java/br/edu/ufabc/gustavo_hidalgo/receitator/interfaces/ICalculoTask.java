package br.edu.ufabc.gustavo_hidalgo.receitator.interfaces;

import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;

/**
 * Created by hdant on 22/04/2017.
 */

public interface ICalculoTask {
    void onPostExecute();
}
