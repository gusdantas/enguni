package br.edu.ufabc.gustavo_hidalgo.receitator.tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import br.edu.ufabc.gustavo_hidalgo.receitator.constants.InfoBase;
import br.edu.ufabc.gustavo_hidalgo.receitator.fragments.TelaInicialFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.ICalculoTask;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Ingrediente;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;
import br.edu.ufabc.gustavo_hidalgo.receitator.utils.FileUtils;

/**
 * Created by hdant on 21/04/2017.
 */

public class CalculoTask extends AsyncTask<Receita, Void, Receita> {
    Activity mActivity;
    ICalculoTask mICalculoTask;
    JSONObject mTaco, mJsonObject;
    Gson mGson;
    File mArquivoReceitasSalvas;
    String mReceitasSalvas;
    boolean isFirst = false;
    public static final String CLOSE_JSON = "]}\"";
    public static final String TAG = "[Rctt]Calc";

    public CalculoTask(ICalculoTask iCalculoTask, Activity activity) {
        super();
        this.mICalculoTask = iCalculoTask;
        this.mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mArquivoReceitasSalvas = new File(Environment.getExternalStorageDirectory() +
                File.separator + TelaInicialFragment.RECEITAS_SALVAS);
        FileUtils.scanFile(mArquivoReceitasSalvas, mActivity);
    }

    @Override
    protected Receita doInBackground(Receita... receitas) {
        try {
            mTaco = new JSONObject(FileUtils.loadJSONFromFile(mActivity, "tacodict.json", true));
            mReceitasSalvas = FileUtils
                    .loadJSONFromFile(mActivity, mArquivoReceitasSalvas.getPath(), false);
            if(mReceitasSalvas == null || mReceitasSalvas.equals("")){
                mReceitasSalvas = "{'Receitas':[";
                isFirst = true;
            }
            mGson = new Gson();
            Log.d(TAG, "debug");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (Ingrediente ingrediente:receitas[0].getIngredientes()) {
            double infoBase, infoIng;
            String str = "";
            for (InfoBase constante: InfoBase.values()) {
                try {
                    str = mTaco.getJSONObject(String.valueOf(ingrediente.getID()))
                            .getString(constante.getRotuloBase())
                            .replace("'", "").replace(",", ".");
                    if (str != null) {
                        switch (str) {
                            case "NA":
                                infoBase = 0.000000000001;
                                break;
                            case "Tr":
                                infoBase = 0.000000000001;
                                break;
                            default:
                                infoBase = Double.parseDouble(str);
                                if(infoBase == 0){
                                    infoBase = 0.000000000001;
                                }
                                break;
                        }
                    } else {
                        infoBase = 0.000000000001;
                    }

                    infoIng = (infoBase * ingrediente.getQuantidade()) / 100;
                    ingrediente.setInfo(constante, infoIng);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        receitas[0].calcInfoTotal();

        if (isFirst) {
            mReceitasSalvas = mReceitasSalvas + mGson.toJson(receitas[0]) + "]}";
        } else {
            mReceitasSalvas = mReceitasSalvas.substring(0, mReceitasSalvas.length()-1);
            mReceitasSalvas = mReceitasSalvas.substring(0, mReceitasSalvas.length()-1);
            mReceitasSalvas = mReceitasSalvas + ","+ mGson.toJson(receitas[0]) + CLOSE_JSON;
        }
        try {
            mJsonObject = new JSONObject(mReceitasSalvas);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        FileUtils.writeToFile(mJsonObject.toString(), mArquivoReceitasSalvas, mActivity);
        return receitas[0];
    }

    @Override
    protected void onPostExecute(Receita receita) {
        super.onPostExecute(receita);
        mICalculoTask.onPostExecute();
    }
}
