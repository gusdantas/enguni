package br.edu.ufabc.gustavo_hidalgo.receitator.constants;

/**
 * Created by hdant on 22/04/2017.
 */

public enum InfoBase {
    ENERGIA("Energia", "kcal"),
    CALCIO("Cálcio", "mg"),
    SODIO("Sódio", "mg"),
    FERRO("Ferro", "mg"),
    PROTEINA("Proteína", "g"),
    LIPIDEO("Lipídeo", "g"),
    CARBO("Carboidrato", "g"),
    FIBRA("Fibra", "g");

    private String mRotuloBase, mUnidade;

    InfoBase(String rotulo, String unidade){
        this.mRotuloBase = rotulo;
        this.mUnidade = unidade;
    }

    public String getRotuloBase(){
        return this.mRotuloBase;
    }

    public String getUnidade(){
        return this.mUnidade;
    }
}
