package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.adapters.IngredienteAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IMainActivity;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;

public class ReceitaProntaFragment extends Fragment implements View.OnClickListener {
    TextView mNomeReceita, mQtsReceita;
    Button mVoltar, mSalvar;
    static Receita mReceita;
    IMainActivity mIMainActivity;
    private RecyclerView mReceitaView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static final String TAG = "[Rectt]RPFr";

    public ReceitaProntaFragment() {
        // Required empty public constructor
    }

    public static ReceitaProntaFragment newInstance(Receita receita) {
        ReceitaProntaFragment fragment = new ReceitaProntaFragment();
        mReceita = receita;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"created");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receita_pronta, container, false);
        Log.d(TAG,"");

        mNomeReceita = (TextView) view.findViewById(R.id.nomeRecPTV);
        mQtsReceita = (TextView) view.findViewById(R.id.qtsTextView);
        mReceitaView = (RecyclerView) view.findViewById(R.id.receitaRV);
        mVoltar = (Button) view.findViewById(R.id.voltarButton);
        mSalvar = (Button) view.findViewById(R.id.prontoButton);
        // Inflate the layout for this fragment

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mReceitaView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new IngredienteAdapter(mReceita.getIngredientes(), Constantes.RECEITA_PRONTA);
        mReceitaView.setAdapter(mAdapter);
        mReceitaView.setHasFixedSize(true);

        mNomeReceita.setText("Receita: "+mReceita.getNome());
        mQtsReceita.setText("Serve "+String.valueOf(mReceita.getServeQts())+" pessoas.");
        mVoltar.setOnClickListener(this);
        mSalvar.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.prontoButton:
                changeFragment(Constantes.RECEITA_CALCULADA, mReceita);
                break;
            case R.id.voltarButton:
                Log.d(TAG,"oC voltar");
                changeFragment(Constantes.RECEITA_ING, mReceita);
                break;
        }

    }

    public void changeFragment(int data, Receita receita) {
        mIMainActivity.changeFragment(data, receita);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mIMainActivity = (IMainActivity) activity;
    }
}
