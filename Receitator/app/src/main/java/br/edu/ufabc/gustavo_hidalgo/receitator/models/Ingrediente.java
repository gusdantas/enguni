package br.edu.ufabc.gustavo_hidalgo.receitator.models;

import org.json.JSONException;
import org.json.JSONObject;

import br.edu.ufabc.gustavo_hidalgo.receitator.constants.InfoBase;

/**
 * Created by hdant on 26/03/2017.
 */

public class Ingrediente {

    private int mQuantidade;
    private String mNome;
    private int mID;
    private double[] mInfo;

    public Ingrediente(int quantidade, String nome, int id) {
        this.mQuantidade = quantidade;
        this.mNome = nome;
        this.mID = id;
        this.mInfo = new double[8];
    }

    public Ingrediente(JSONObject ingrediente) throws JSONException {
        this.mQuantidade = (int) ingrediente.get("mQuantidade");
        this.mNome = (String) ingrediente.get("mNome");
        this.mID = (int) ingrediente.get("mID");
        this.mInfo = (double[]) ingrediente.get("mInfo");
    }

    public String getNome() {
        return mNome;
    }

    public void setNome(String nome) {
        this.mNome = nome;
    }

    public int getQuantidade() {
        return mQuantidade;
    }

    public void setQuantidade(int qtd) {
        this.mQuantidade = qtd;
    }

    public int getID() {
        return mID;
    }

    public void setID(int id) {
        this.mID = id;
    }

    public double getInfo(InfoBase constante){
        return mInfo[constante.ordinal()];
    }

    public void setInfo(InfoBase constante, double valor){
        this.mInfo[constante.ordinal()] = valor;
    }


}
