package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.adapters.IngredienteAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.InfoBase;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;

public class InfoTotalFragment extends Fragment {
    TextView mEnergia, mCarbo, mProteina, mLipideo, mFibra, mSodio, mCalcio;
    static Receita mReceita;
    private BroadcastReceiver mMessageReceiver;
    NumberFormat mNumberFormat;

    public static final String TAG = "[Rectt]RRFr";

    public InfoTotalFragment() {
        // Required empty public constructor
    }

    public static InfoTotalFragment newInstance(Receita receita) {
        InfoTotalFragment fragment = new InfoTotalFragment();
        mReceita = receita;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"created");
        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra("message");
                Log.d("receiver", "Got message: " + message);
                setTexts();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));

    }

    private void setTexts() {
        mEnergia.setText(mNumberFormat.format(mReceita.getInfoTotal()[InfoBase.ENERGIA.ordinal()])+
                InfoBase.ENERGIA.getUnidade());
        mCarbo.setText(mNumberFormat.format(mReceita.getInfoTotal()[InfoBase.CARBO.ordinal()])+
                InfoBase.CARBO.getUnidade());
        mProteina.setText(mNumberFormat.format(mReceita.getInfoTotal()[InfoBase.PROTEINA.ordinal()])+
                InfoBase.PROTEINA.getUnidade());
        mLipideo.setText(mNumberFormat.format(mReceita.getInfoTotal()[InfoBase.LIPIDEO.ordinal()])+
                InfoBase.LIPIDEO.getUnidade());
        mFibra.setText(mNumberFormat.format(mReceita.getInfoTotal()[InfoBase.FIBRA.ordinal()])+
                InfoBase.FIBRA.getUnidade());
        mSodio.setText(mNumberFormat.format(mReceita.getInfoTotal()[InfoBase.SODIO.ordinal()])+
                InfoBase.SODIO.getUnidade());
        mCalcio.setText(mNumberFormat.format(mReceita.getInfoTotal()[InfoBase.CALCIO.ordinal()])+
                InfoBase.CALCIO.getUnidade());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_total, container, false);
        Log.d(TAG,"");

        mEnergia = (TextView) view.findViewById(R.id.energiaValorTextView);
        mCarbo = (TextView) view.findViewById(R.id.carboValorTextView);
        mProteina = (TextView) view.findViewById(R.id.proteinaValorTextView);
        mLipideo = (TextView) view.findViewById(R.id.lipideoValorTextView);
        mFibra = (TextView) view.findViewById(R.id.fibraValorTextView);
        mSodio = (TextView) view.findViewById(R.id.sodioValorTextView);
        mCalcio = (TextView) view.findViewById(R.id.calcioValorTextView);
        // Inflate the layout for this fragment

        mNumberFormat = NumberFormat.getNumberInstance(new Locale("pt","BR"));
        mNumberFormat.setMaximumFractionDigits(3);
        setTexts();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }
}
