package br.edu.ufabc.gustavo_hidalgo.receitator.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hdant on 26/04/2017.
 */

public interface IReceitasAdapter {
    void onPositive(JSONObject receita);
    void onNegative(ArrayList<String> listaReceitas, JSONArray jsonArray);
}
