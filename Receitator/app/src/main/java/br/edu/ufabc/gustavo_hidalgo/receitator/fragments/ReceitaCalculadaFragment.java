package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.google.gson.Gson;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.ICalculoTask;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IMainActivity;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IReceitaCalculadaFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;
import br.edu.ufabc.gustavo_hidalgo.receitator.tasks.CalculoTask;
import br.edu.ufabc.gustavo_hidalgo.receitator.utils.FileUtils;

public class ReceitaCalculadaFragment extends Fragment implements IMainActivity, ICalculoTask,
        View.OnClickListener {

    static Receita mReceita;
    Receita mReceitaDin;
    FrameLayout mInfoLayout;
    Button mVoltar, mSalvar;
    BottomNavigationView mMenuNavigationView;
    IMainActivity mIMainActivity;
    CalculoTask mCalculoTask;
    public android.app.Fragment mFragment;
    public FragmentManager mFragmentManager;
    public FragmentTransaction mFragmentTransactionInt, mFragmentTransactionOut;
    public static final String TAG = "[Rctt]RCFr";

    //private OnFragmentInteractionListener mListener;

    public ReceitaCalculadaFragment() {
        // Required empty public constructor
    }

    public static ReceitaCalculadaFragment newInstance(Receita receita) {
        ReceitaCalculadaFragment fragment = new ReceitaCalculadaFragment();
        mReceita = receita;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReceitaDin = mReceita;
        mCalculoTask = new CalculoTask(this, getActivity());
        mCalculoTask.execute(mReceitaDin);
        mFragmentManager = getFragmentManager();
        changeFragment(Constantes.INFO_RECEITA, mReceita);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_receita_calculada, container, false);

        mInfoLayout = (FrameLayout) view.findViewById(R.id.infoFrameLayout);
        mVoltar = (Button) view.findViewById(R.id.voltarButton);
        mSalvar = (Button) view.findViewById(R.id.salvarButton);
        mMenuNavigationView = (BottomNavigationView) view.findViewById(R.id.bottom_navigation);

        mVoltar.setOnClickListener(this);
        mSalvar.setOnClickListener(this);

        mMenuNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.infoReceita:
                        changeFragment(Constantes.INFO_RECEITA, mReceita);
                        break;
                    case R.id.infoTotal:
                        changeFragment(Constantes.INFO_TOTAL, mReceita);
                        break;
                    case R.id.infoPorção:
                        changeFragment(Constantes.INFO_PORCAO, mReceita);
                        break;
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void changeFragment(int fragment, Receita receita){
        mFragmentTransactionInt = mFragmentManager.beginTransaction();
        Log.d(TAG,String.valueOf(fragment)+" - changing frag");
        switch (fragment){
            case Constantes.INFO_RECEITA:
                mFragment = InfoReceitaFragment.newInstance(mReceita);
                break;
            case Constantes.INFO_TOTAL:
                mFragment = InfoTotalFragment.newInstance(mReceita);
                break;
            case Constantes.INFO_PORCAO:
                mFragment = InfoPorcaoFragment.newInstance(mReceita);
                break;
        }
        mFragmentTransactionInt.replace(R.id.infoFrameLayout, mFragment);
        mFragmentTransactionInt.commit();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mIMainActivity = (IMainActivity) activity;
    }

    @Override
    public void onClick(View v) {
        mFragmentTransactionOut = mFragmentManager.beginTransaction();
        mFragmentTransactionOut.remove(mFragment).commit();
        switch (v.getId()){
            case R.id.salvarButton:
                //FileUtils.salvaReceita(mReceita, getActivity());
                mIMainActivity.changeFragment(Constantes.TELA_INICIAL, null);
                break;
            case R.id.voltarButton:
                Log.d(TAG,"oC voltar");
                mIMainActivity.changeFragment(Constantes.RECEITA_PRONTA, mReceita);
                break;
        }
    }

    @Override
    public void onPostExecute() {
        mReceita = mReceitaDin;
        sendMessage();
    }

    private void sendMessage() {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent("custom-event-name");
        // You can also include some extra data.
        intent.putExtra("message", "This is my message!");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }




    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}
