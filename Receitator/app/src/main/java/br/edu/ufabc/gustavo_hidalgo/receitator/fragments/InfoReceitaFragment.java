package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.adapters.IngredienteAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.InfoBase;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.ICalculoTask;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IMainActivity;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IReceitaCalculadaFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;
import br.edu.ufabc.gustavo_hidalgo.receitator.tasks.CalculoTask;
import br.edu.ufabc.gustavo_hidalgo.receitator.utils.FileUtils;

public class InfoReceitaFragment extends Fragment {
    TextView mNomeReceita, mQtsReceita;
    static Receita mReceita;
    //CalculoTask mCalculoTask;
    private BroadcastReceiver mMessageReceiver;
    NumberFormat mNumberFormat;
    private RecyclerView mReceitaView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static final String TAG = "[Rectt]RRFr";

    public InfoReceitaFragment() {
        // Required empty public constructor
    }

    public static InfoReceitaFragment newInstance(Receita receita) {
        InfoReceitaFragment fragment = new InfoReceitaFragment();
        mReceita = receita;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"created");
        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra("message");
                Log.d("receiver", "Got message: " + message);
                mAdapter.notifyDataSetChanged();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));
        //mCalculoTask = new CalculoTask(this, getActivity());
        //mCalculoTask.execute(mReceita);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_receita, container, false);
        Log.d(TAG,"");

        mNomeReceita = (TextView) view.findViewById(R.id.nomeRecPTV);
        mQtsReceita = (TextView) view.findViewById(R.id.qtsTextView);
        mReceitaView = (RecyclerView) view.findViewById(R.id.receitaRV);
        // Inflate the layout for this fragment

        mNumberFormat = NumberFormat.getNumberInstance(new Locale("pt","BR"));
        mNumberFormat.setMaximumFractionDigits(3);


        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mReceitaView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new IngredienteAdapter(mReceita.getIngredientes(), Constantes.RECEITA_CALCULADA);
        mReceitaView.setAdapter(mAdapter);
        mReceitaView.setHasFixedSize(true);

        mNomeReceita.setText(mReceita.getNome());
        mQtsReceita.setText("Serve "+String.valueOf(mReceita.getServeQts())+" pessoas.");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }
}
