package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.List;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.adapters.BuscaAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IBuscaDialogFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Ingrediente;
import br.edu.ufabc.gustavo_hidalgo.receitator.utils.FileUtils;


public class BuscaDialogFragment extends DialogFragment implements SearchView.OnQueryTextListener,
        AdapterView.OnItemClickListener, View.OnClickListener {
    /** The system calls this to get the DialogFragment's layout, regardless
     of whether it's being displayed as a dialog or an embedded fragment. */
    EditText mQtdIng;
    SearchView mBuscaIng;
    ListView mBuscaListView;
    BuscaAdapter mBuscaAdapter;
    Ingrediente mIngrediente;
    static IBuscaDialogFragment mIBuscaDialogFragment;
    static List<Ingrediente> mListaIngrediente;
    public static final String TAG = "[Rectt]BDFr";

    public BuscaDialogFragment(){
    }

    public static BuscaDialogFragment newInstance(List<Ingrediente> lista,
                                                  IBuscaDialogFragment iBuscaDialogFragment) {
        BuscaDialogFragment fragment = new BuscaDialogFragment();
        mIBuscaDialogFragment = iBuscaDialogFragment;
        mListaIngrediente = lista;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_busca, container, false);

        mQtdIng = (EditText) view.findViewById(R.id.qtdEditText);

        mBuscaIng = (SearchView) view.findViewById(R.id.alimentoSearchView);
        mBuscaListView = (ListView) view.findViewById(R.id.alimentoListView);

        mBuscaAdapter = new BuscaAdapter(getActivity(), FileUtils.indiceAlimento());

        mBuscaListView.setAdapter(mBuscaAdapter);
        mBuscaListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        mBuscaListView.setOnItemClickListener(this);
        mBuscaIng.setOnQueryTextListener(this);
        mBuscaIng.setOnClickListener(this);

        // Inflate the layout to use as dialog or embedded fragment
        return view;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d(TAG,"oQTS");
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d(TAG,"oQTC= "+newText);
        mBuscaAdapter.filter(newText);
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mIngrediente = new Ingrediente(Integer.parseInt(mQtdIng.getText().toString()),
                mBuscaAdapter.getItem(position), position);
        Log.d(TAG, String.valueOf(mIngrediente.getQuantidade())+"g de "+ mIngrediente.getNome());
        mListaIngrediente.add(mIngrediente);
        dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alimentoSearchView:
                mBuscaIng.onActionViewExpanded();
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mIBuscaDialogFragment.onDismiss();
    }
}
