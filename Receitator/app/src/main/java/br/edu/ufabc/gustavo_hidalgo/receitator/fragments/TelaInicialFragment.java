package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.adapters.IngredienteAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.adapters.ReceitasAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IMainActivity;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IReceitasAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;
import br.edu.ufabc.gustavo_hidalgo.receitator.utils.FileUtils;

public class TelaInicialFragment extends Fragment implements View.OnClickListener, IReceitasAdapter{

    Button mNovaReceitaBt;
    RecyclerView mSalvasRecyclerView;
    RecyclerView.Adapter mSalvasAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    IMainActivity mIMainActivity;
    static JSONArray mJsonArray;
    static ArrayList<String> mReceitasSalvas;
    public static final String TAG = "[Rectt]TelaInicialFragment";

    public TelaInicialFragment() {
        // Required empty public constructor
    }

    public static TelaInicialFragment newInstance(JSONArray jsonArray, ArrayList<String> arrayList){
        TelaInicialFragment fragment = new TelaInicialFragment();
        mJsonArray = jsonArray;
        mReceitasSalvas = arrayList;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tela_inicial, container, false);

        mNovaReceitaBt = (Button) view.findViewById(R.id.novaReceitaBt);
        mSalvasRecyclerView = (RecyclerView) view.findViewById(R.id.salvasRecyclerView);






        mLayoutManager = new LinearLayoutManager(getActivity());
        mSalvasRecyclerView.setLayoutManager(mLayoutManager);
        mSalvasAdapter = new ReceitasAdapter(mReceitasSalvas, mJsonArray, this);
        mSalvasRecyclerView.setAdapter(mSalvasAdapter);

        mNovaReceitaBt.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.novaReceitaBt:
                changeFragment(Constantes.RECEITA_NOME, null);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mIMainActivity = (IMainActivity) activity;
    }

    public void changeFragment(int data, Receita receita) {
        mIMainActivity.changeFragment(data, receita);
    }

    @Override
    public void onPositive(JSONObject receita) {
        Receita mReceita = null;
        try {
            mReceita = new Receita(receita);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        changeFragment(Constantes.RECEITA_CALCULADA, mReceita);
    }

    @Override
    public void onNegative(ArrayList<String> listaReceitas, JSONArray jsonArray) {
        mReceitasSalvas = listaReceitas;
        mJsonArray = jsonArray;
        String string = "{\"Receitas\" : "+mJsonArray.toString()+"}";
        FileUtils.writeToFile(string, mArquivoReceitasSalvas, getActivity());
    }
}
