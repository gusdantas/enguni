package br.edu.ufabc.gustavo_hidalgo.receitator.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.edu.ufabc.gustavo_hidalgo.receitator.constants.InfoBase;

/**
 * Created by hdant on 27/03/2017.
 */

public class Receita {
    String mNome;
    List<Ingrediente> mIngredientes;
    int mServeQts;
    double[] mInfoTotal, mInfoPorcao;

    public Receita(){
        this.mIngredientes = new ArrayList<>();
        this.mInfoTotal = new double[8];
        this.mInfoPorcao = new double[8];
    }

    public Receita(JSONObject receita) throws JSONException {
        this.mInfoTotal = (double[]) receita.get("mInfoTotal");
        this.mInfoPorcao = (double[]) receita.get("mInfoPorcao");
        this.mNome = (String) receita.get("mNome");
        this.mServeQts = (int) receita.get("mServeQts");
        JSONArray ing = new JSONArray(receita.get("mIngredientes").toString());
        for (int i = 0; i < ing.length(); i++) {
            mIngredientes.add(new Ingrediente(new JSONObject(ing.get(i).toString())));
        }
    }

    public String getNome() {
        return mNome;
    }

    public void setNome(String nome) {
        this.mNome = nome;
    }

    public List<Ingrediente> getIngredientes() {
        return mIngredientes;
    }

    public void setIngredientes(List<Ingrediente> ingredientes) {
        this.mIngredientes = ingredientes;
    }

    public int getServeQts() {
        return mServeQts;
    }

    public void setServeQts(int serveQts) {
        this.mServeQts = serveQts;
    }

    public double[] getInfoTotal() {
        return mInfoTotal;
    }

    public void setInfoTotal(double[] infoTotal) {
        this.mInfoTotal = infoTotal;
    }

    public double[] getInfoPorcao() {
        return mInfoPorcao;
    }

    public void setInfoPorcao(double[] infoPorcao) {
        this.mInfoPorcao = infoPorcao;
    }

    public double getInfo(InfoBase constante){
        double info = 0;
        for (Ingrediente ingrediente:mIngredientes) {
            info += ingrediente.getInfo(constante);
        }
        return info;
    }

    public void calcInfoTotal(){
        for (InfoBase constante: InfoBase.values()){
            mInfoTotal[constante.ordinal()] = getInfo(constante);
            mInfoPorcao[constante.ordinal()] = mInfoTotal[constante.ordinal()]/mServeQts;
        }
    }
}
