package br.edu.ufabc.gustavo_hidalgo.receitator.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.InfoBase;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Ingrediente;

/**
 * Created by hdant on 26/03/2017.
 */

public class IngredienteAdapter extends
        RecyclerView.Adapter<IngredienteAdapter.IngredienteViewHolder> {

    static List<Ingrediente> mListaIngrediente;
    NumberFormat mNumberFormat;
    int mFragment;

    public IngredienteAdapter(List<Ingrediente> listaIngrediente, int fragment) {
        mListaIngrediente = listaIngrediente;
        this.mFragment = fragment;
        mNumberFormat = NumberFormat.getNumberInstance(new Locale("pt","BR"));
        mNumberFormat.setMaximumFractionDigits(3);
    }

    @Override
    public int getItemCount() {
        return mListaIngrediente.size();
    }

    @Override
    public void onBindViewHolder(IngredienteViewHolder ingredienteViewHolder, int i) {
        final int position = i;
        final Ingrediente ci = mListaIngrediente.get(position);
        ingredienteViewHolder.mIngQtd.setText(String.valueOf(ci.getQuantidade()));
        ingredienteViewHolder.mIngNome.setText(ci.getNome());
        switch (mFragment){
            case Constantes.RECEITA_ING:
                ingredienteViewHolder.mIngRemove.setVisibility(View.VISIBLE);
                ingredienteViewHolder.mIngRemove.setChecked(true);
                ingredienteViewHolder.mIngRemove.setClickable(true);
                ingredienteViewHolder.mIngRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                        builder.setTitle(ci.getNome())
                                .setMessage("Remover Ingrediente?")
                                .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mListaIngrediente.remove(position);
                                        notifyDataSetChanged();
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton("NÂO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }
                });
                break;
            case Constantes.RECEITA_CALCULADA:
                ingredienteViewHolder.mIngEnergia.setVisibility(View.VISIBLE);
                ingredienteViewHolder.mIngEnergia
                        .setText(mNumberFormat.format(ci.getInfo(InfoBase.ENERGIA)));
                break;
        }
    }

    @Override
    public IngredienteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.ingrediente_adapter, viewGroup, false);

        return new IngredienteViewHolder(itemView);
    }

    public static class IngredienteViewHolder extends RecyclerView.ViewHolder {
        protected TextView mIngQtd, mIngEnergia, mIngNome;
        protected CheckBox mIngRemove;

        public IngredienteViewHolder(View v) {
            super(v);
            mIngQtd = (TextView) v.findViewById(R.id.receitaTextView);
            mIngNome = (TextView) v.findViewById(R.id.ingNome);
            mIngRemove = (CheckBox) v.findViewById(R.id.ingRemove);
            mIngEnergia = (TextView) v.findViewById(R.id.ingEnergia);
        }
    }
}
