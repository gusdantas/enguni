package br.edu.ufabc.gustavo_hidalgo.receitator.constants;

/**
 * Created by hdant on 23/04/2017.
 */

public class Constantes {
    public static final int TELA_INICIAL = 0;
    public static final int RECEITA_NOME = 1;
    public static final int RECEITA_ING = 2;
    public static final int RECEITA_PRONTA = 3;
    public static final int RECEITA_CALCULADA = 4;
    public static final int INFO_RECEITA = 5;
    public static final int INFO_TOTAL = 6;
    public static final int INFO_PORCAO = 7;
}
