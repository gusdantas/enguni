package br.edu.ufabc.gustavo_hidalgo.receitator.interfaces;

import org.json.JSONArray;

import java.util.ArrayList;

import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;

/**
 * Created by hdant on 26/03/2017.
 */

public interface IMainActivity {
    void changeFragment(int fragment, JSONArray jsonArray, ArrayList<String> arrayList);
    void changeFragment(int fragment, Receita receita);
}
