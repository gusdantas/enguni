package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.adapters.IngredienteAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IBuscaDialogFragment;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IMainActivity;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Ingrediente;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;

public class ReceitaIngFragment extends Fragment implements View.OnClickListener,
        IBuscaDialogFragment {

    TextView mNomeReceita, mServeQts;
    RecyclerView mIngRecyclerView;
    RecyclerView.Adapter mIngAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Button mRecPronto, mAdicionaIng, mVoltar;
    BuscaDialogFragment mBuscaDialogFragment;

    List<Ingrediente> mListaIngredientes;
    static Receita mReceita;
    private IMainActivity mIMainActivity;
    public static final String TAG = "[Rectt]NRIFr";

    public ReceitaIngFragment() {
        // Required empty public constructor
    }

    public static ReceitaIngFragment newInstance(Receita receita) {
        ReceitaIngFragment fragment = new ReceitaIngFragment();
        mReceita = receita;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "create");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "view");
        View view = inflater.inflate(R.layout.fragment_receita_ing, container, false);

        mNomeReceita = (TextView) view.findViewById(R.id.nomeRecTextView);
        mServeQts = (TextView) view.findViewById(R.id.serveTextView);
        mAdicionaIng = (Button) view.findViewById(R.id.adicionarIngBt);
        mIngRecyclerView = (RecyclerView) view.findViewById(R.id.ingRV);
        mRecPronto = (Button) view.findViewById(R.id.ingRecOKBt);
        mVoltar = (Button) view.findViewById(R.id.voltarButton);

        mNomeReceita.setText("Receita: "+mReceita.getNome());
        mServeQts.setText("Serve "+String.valueOf(mReceita.getServeQts())+" pessoas.");

        mIngRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mIngRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mListaIngredientes = mReceita.getIngredientes();
        mIngAdapter = new IngredienteAdapter(mListaIngredientes, Constantes.RECEITA_ING);
        mIngRecyclerView.setAdapter(mIngAdapter);

        //mListaIngredientes.clear();
        mAdicionaIng.setOnClickListener(this);
        mRecPronto.setOnClickListener(this);
        mVoltar.setOnClickListener(this);
        // Inflate the layout for this fragment
        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adicionarIngBt:
                Log.d(TAG,"oC= button");
                showDialog();
                break;
            case R.id.ingRecOKBt:
                Log.d(TAG,"oC= ingRecOKBt");
                mReceita.setIngredientes(mListaIngredientes);
                changeFragment(Constantes.RECEITA_PRONTA, mReceita);
                break;
            case R.id.voltarButton:
                Log.d(TAG,"oC voltar");
                changeFragment(Constantes.RECEITA_NOME, mReceita);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mIMainActivity = (IMainActivity) activity;
    }

    public void changeFragment(int data, Receita receita) {
        mIMainActivity.changeFragment(data, receita);
    }

    public void showDialog() {
        Log.d(TAG, "showDialog");
        FragmentManager fragmentManager = getFragmentManager();
        mBuscaDialogFragment = BuscaDialogFragment.newInstance(mListaIngredientes, this);
        mBuscaDialogFragment.show(fragmentManager, "dialog");
    }

    @Override
    public void onDismiss() {
        mIngAdapter.notifyDataSetChanged();
    }
}
