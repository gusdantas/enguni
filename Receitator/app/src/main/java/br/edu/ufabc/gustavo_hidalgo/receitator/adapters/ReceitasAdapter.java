package br.edu.ufabc.gustavo_hidalgo.receitator.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.InfoBase;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IReceitasAdapter;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Ingrediente;

/**
 * Created by hdant on 26/03/2017.
 */

public class ReceitasAdapter extends
        RecyclerView.Adapter<ReceitasAdapter.IngredienteViewHolder> {

    static ArrayList<String> mListaReceitas;
    static JSONArray mJsonArray;
    IReceitasAdapter mIReceitasAdapter;

    public ReceitasAdapter(ArrayList<String> listaIngrediente, JSONArray jsonArray,
                           IReceitasAdapter iReceitasAdapter) {
        mListaReceitas = listaIngrediente;
        mJsonArray = jsonArray;
        mIReceitasAdapter = iReceitasAdapter;
    }

    @Override
    public int getItemCount() {
        return mListaReceitas.size();
    }

    @Override
    public void onBindViewHolder(IngredienteViewHolder ingredienteViewHolder, int i) {
        final int position = i;
        final String ci = mListaReceitas.get(position);
        ingredienteViewHolder.mReceita.setText(ci);
        ingredienteViewHolder.mReceita.setClickable(true);
        ingredienteViewHolder.mReceita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle(ci)
                        .setMessage("Ver ou deletar receita?")
                        .setPositiveButton("VER", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                JSONObject receita = new JSONObject();
                                try {
                                    receita = new JSONObject(mJsonArray.get(position).toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mIReceitasAdapter.onPositive(receita);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("DELETAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mListaReceitas.remove(position);
                                mJsonArray.remove(position);
                                notifyDataSetChanged();
                                mIReceitasAdapter.onNegative(mListaReceitas, mJsonArray);
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public IngredienteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.receitas_adapter, viewGroup, false);

        return new IngredienteViewHolder(itemView);
    }

    public static class IngredienteViewHolder extends RecyclerView.ViewHolder {
        protected TextView mReceita;

        public IngredienteViewHolder(View v) {
            super(v);
            mReceita = (TextView) v.findViewById(R.id.receitaTextView);
        }
    }
}
