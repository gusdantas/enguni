package br.edu.ufabc.gustavo_hidalgo.receitator.utils;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.SparseArray;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import br.edu.ufabc.gustavo_hidalgo.receitator.models.Ingrediente;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;

/**
 * Created by juliana.hayashi on 05/04/2017.
 */

public final class FileUtils {
    public static final String TAG = "[Rec]FileUtils";

    public static String loadJSONFromFile(Context context, String filename, boolean isAsset) {
        String json = null;
        try {
            InputStream is;
            if(isAsset) {
                is = context.getAssets().open(filename);
            } else {
                is = new FileInputStream(filename);
            }

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static void salvaReceita(Receita receita, Context context) {
        Log.d(TAG,"salvaReceita");
        String receitaSt = receita.getNome()+"\n\nIngredientes:\n";
        for (Ingrediente ingrediente : receita.getIngredientes()) {
            receitaSt = receitaSt + ingrediente.getQuantidade() + " - " + ingrediente.getNome() + "\n";
        }
        File file = new File(Environment.getExternalStorageDirectory() +
                File.separator + receita.getNome());
        FileUtils.writeToFile(receitaSt, file, context);
    }

    public static void writeToFile(String data, File file, Context context) {
        checkFile(file);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data.getBytes());
            fileOutputStream.close();
            Log.i(TAG, "writeToFile: ");
        }
        catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }
        scanFile(file, context);
    }

    public static void readFromFile(String string, File file, Context context) {
        int buffer = 1024;
        int offset = 0;
        byte[] byteBuffer = new byte[buffer];
        int length = (int) file.length();
        //checkFile(file);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);

            while(offset < length){
                string = string+fileInputStream.read(byteBuffer, offset, length);
                offset = offset+buffer;
            }

            fileInputStream.close();

            Log.i(TAG, "writeToFile: ");
        }
        catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }
        scanFile(file, context);
    }

    public static void checkFile(File file){
        if (file.exists()){
            file.delete();
        }
    }

    public static void scanFile(File file, Context context){
        MediaScannerConnection.scanFile(
                context,
                new String[]{file.getAbsolutePath()},
                null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Log.v(TAG,
                                "file " + path + " was scanned seccessfully: " + uri);
                    }
                });
    }

    public static void rawToString(InputStream inputStream, String string){
        int buffer = 1024;
        int offset = 0;
        byte[] byteBuffer = new byte[buffer];
        int length = 0;
        try {
            length = inputStream.available();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //checkFile(file);
        try {
            while(offset+buffer < length){
                string = string + inputStream.read(byteBuffer, offset, length);
                offset = offset + buffer;
            }
            while(offset < length){
                string = string + inputStream.read(byteBuffer, offset, length);
                offset++;
            }
            Log.i(TAG, "writeToFile: ");
        }
        catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }

    }

    public static ArrayList<String> indiceAlimento() {
        ArrayList<String> sparseArray = new ArrayList<>();

        sparseArray.add("Arroz, integral, cozido");
        sparseArray.add("Arroz, integral, cru ");
        sparseArray.add("Arroz, tipo 1, cozido");
        sparseArray.add("Arroz, tipo 1, cru ");
        sparseArray.add("Arroz, tipo 2, cozido");
        sparseArray.add("Arroz, tipo 2, cru ");
        sparseArray.add("Aveia, flocos, crua");
        sparseArray.add("Biscoito, doce, maisena");
        sparseArray.add("Biscoito, doce, recheado com chocolate ");
        sparseArray.add("Biscoito, doce, recheado com morango");
        sparseArray.add("Biscoito, doce, wafer, recheado de chocolate");
        sparseArray.add("Biscoito, doce, wafer, recheado de morango");
        sparseArray.add("Biscoito, salgado, cream cracker");
        sparseArray.add("Bolo, mistura para");
        sparseArray.add("Bolo, pronto, aipim ");
        sparseArray.add("Bolo, pronto, chocolate ");
        sparseArray.add("Bolo, pronto, coco");
        sparseArray.add("Bolo, pronto, milho ");
        sparseArray.add("Canjica, branca, crua ");
        sparseArray.add("Canjica, com leite integral ");
        sparseArray.add("Cereais, milho, flocos, com sal ");
        sparseArray.add("Cereais, milho, flocos, sem sal ");
        sparseArray.add("Cereais, mingau, milho, infantil");
        sparseArray.add("Cereais, mistura para vitamina, trigo, cevada e aveia ");
        sparseArray.add("Cereal matinal, milho ");
        sparseArray.add("Cereal matinal, milho, açúcar ");
        sparseArray.add("Creme de arroz, pó");
        sparseArray.add("Creme de milho, pó");
        sparseArray.add("Curau, milho verde");
        sparseArray.add("Curau, milho verde, mistura para");
        sparseArray.add("Farinha, de arroz, enriquecida");
        sparseArray.add("Farinha, de centeio, integral ");
        sparseArray.add("Farinha, de milho, amarela");
        sparseArray.add("Farinha, de rosca ");
        sparseArray.add("Farinha, de trigo ");
        sparseArray.add("Farinha, láctea, de cereais ");
        sparseArray.add("Lasanha, massa fresca, cozida ");
        sparseArray.add("Lasanha, massa fresca, crua ");
        sparseArray.add("Macarrão, instantâneo ");
        sparseArray.add("Macarrão, trigo, cru");
        sparseArray.add("Macarrão, trigo, cru, com ovos");
        sparseArray.add("Milho, amido, cru ");
        sparseArray.add("Milho, fubá, cru");
        sparseArray.add("Milho, verde, cru ");
        sparseArray.add("Milho, verde, enlatado, drenado ");
        sparseArray.add("Mingau tradicional, pó");
        sparseArray.add("Pamonha, barra para cozimento, pré-cozida ");
        sparseArray.add("Pão, aveia, forma ");
        sparseArray.add("Pão, de soja");
        sparseArray.add("Pão, glúten, forma");
        sparseArray.add("Pão, milho, forma ");
        sparseArray.add("Pão, trigo, forma, integral ");
        sparseArray.add("Pão, trigo, francês ");
        sparseArray.add("Pão, trigo, sovado");
        sparseArray.add("Pastel, de carne, cru ");
        sparseArray.add("Pastel, de carne, frito ");
        sparseArray.add("Pastel, de queijo, cru");
        sparseArray.add("Pastel, de queijo, frito");
        sparseArray.add("Pastel, massa, crua ");
        sparseArray.add("Pastel, massa, frita");
        sparseArray.add("Pipoca, com óleo de soja, sem sal ");
        sparseArray.add("Polenta, pré-cozida ");
        sparseArray.add("Torrada, pão francês");
        sparseArray.add("Abóbora, cabotian, cozida ");
        sparseArray.add("Abóbora, cabotian, crua ");
        sparseArray.add("Abóbora, menina brasileira, crua");
        sparseArray.add("Abóbora, moranga, crua");
        sparseArray.add("Abóbora, moranga, refogada");
        sparseArray.add("Abóbora, pescoço, crua");
        sparseArray.add("Abobrinha, italiana, cozida ");
        sparseArray.add("Abobrinha, italiana, crua ");
        sparseArray.add("Abobrinha, italiana, refogada ");
        sparseArray.add("Abobrinha, paulista, crua ");
        sparseArray.add("Acelga, crua");
        sparseArray.add("Agrião, cru ");
        sparseArray.add("Aipo, cru ");
        sparseArray.add("Alface, americana, crua ");
        sparseArray.add("Alface, crespa, crua");
        sparseArray.add("Alface, lisa, crua");
        sparseArray.add("Alface, roxa, crua");
        sparseArray.add("Alfavaca, crua");
        sparseArray.add("Alho, cru ");
        sparseArray.add("Alho-poró, cru");
        sparseArray.add("Almeirão, cru ");
        sparseArray.add("Almeirão, refogado");
        sparseArray.add("Batata, baroa, cozida ");
        sparseArray.add("Batata, baroa, crua ");
        sparseArray.add("Batata, doce, cozida");
        sparseArray.add("Batata, doce, crua");
        sparseArray.add("Batata, frita, tipo chips, industrializada");
        sparseArray.add("Batata, inglesa, cozida ");
        sparseArray.add("Batata, inglesa, crua ");
        sparseArray.add("Batata, inglesa, frita");
        sparseArray.add("Batata, inglesa, sauté");
        sparseArray.add("Berinjela, cozida ");
        sparseArray.add("Berinjela, crua ");
        sparseArray.add("Beterraba, cozida ");
        sparseArray.add("Beterraba, crua ");
        sparseArray.add("Biscoito, polvilho doce ");
        sparseArray.add("Brócolis, cozido ");
        sparseArray.add("Brócolis, cru");
        sparseArray.add("Cará, cozido ");
        sparseArray.add("Cará, cru");
        sparseArray.add("Caruru, cru");
        sparseArray.add("Catalonha, crua");
        sparseArray.add("Catalonha, refogada");
        sparseArray.add("Cebola, crua ");
        sparseArray.add("Cebolinha, crua");
        sparseArray.add("Cenoura, cozida");
        sparseArray.add("Cenoura, crua");
        sparseArray.add("Chicória, crua ");
        sparseArray.add("Chuchu, cozido ");
        sparseArray.add("Chuchu, cru");
        sparseArray.add("Coentro, folhas desidratadas ");
        sparseArray.add("Couve, manteiga, crua");
        sparseArray.add("Couve, manteiga, refogada");
        sparseArray.add("Couve-flor, crua ");
        sparseArray.add("Couve-flor, cozida ");
        sparseArray.add("Espinafre, Nova Zelândia, cru");
        sparseArray.add("Espinafre, Nova Zelândia, refogado ");
        sparseArray.add("Farinha, de mandioca, crua ");
        sparseArray.add("Farinha, de mandioca, torrada");
        sparseArray.add("Farinha, de puba ");
        sparseArray.add("Fécula, de mandioca");
        sparseArray.add("Feijão, broto, cru ");
        sparseArray.add("Inhame, cru");
        sparseArray.add("Jiló, cru");
        sparseArray.add("Jurubeba, crua ");
        sparseArray.add("Mandioca, cozida ");
        sparseArray.add("Mandioca, crua ");
        sparseArray.add("Mandioca, farofa, temperada");
        sparseArray.add("Mandioca, frita");
        sparseArray.add("Manjericão, cru");
        sparseArray.add("Maxixe, cru");
        sparseArray.add("Mostarda, folha, crua");
        sparseArray.add("Nhoque, batata, cozido ");
        sparseArray.add("Nabo, cru");
        sparseArray.add("Palmito, juçara, em conserva ");
        sparseArray.add("Palmito, pupunha, em conserva");
        sparseArray.add("Pão, de queijo, assado ");
        sparseArray.add("Pão, de queijo, cru");
        sparseArray.add("Pepino, cru");
        sparseArray.add("Pimentão, amarelo, cru ");
        sparseArray.add("Pimentão, verde, cru ");
        sparseArray.add("Pimentão, vermelho, cru");
        sparseArray.add("Polvilho, doce ");
        sparseArray.add("Quiabo, cru");
        sparseArray.add("Rabanete, cru");
        sparseArray.add("Repolho, branco, cru ");
        sparseArray.add("Repolho, roxo, cru ");
        sparseArray.add("Repolho, roxo, refogado");
        sparseArray.add("Rúcula, crua ");
        sparseArray.add("Salsa, crua");
        sparseArray.add("Seleta de legumes, enlatada");
        sparseArray.add("Serralha, crua ");
        sparseArray.add("Taioba, crua ");
        sparseArray.add("Tomate, com semente, cru ");
        sparseArray.add("Tomate, extrato");
        sparseArray.add("Tomate, molho industrializado");
        sparseArray.add("Tomate, purê ");
        sparseArray.add("Tomate, salada ");
        sparseArray.add("Vagem, crua");
        sparseArray.add("Abacate, cru ");
        sparseArray.add("Abacaxi, cru ");
        sparseArray.add("Abacaxi, polpa, congelada");
        sparseArray.add("Abiu, cru");
        sparseArray.add("Açaí, polpa, com xarope de guaraná e glucose ");
        sparseArray.add("Açaí, polpa, congelada ");
        sparseArray.add("Acerola, crua");
        sparseArray.add("Acerola, polpa, congelada");
        sparseArray.add("Ameixa, calda, enlatada");
        sparseArray.add("Ameixa, crua ");
        sparseArray.add("Ameixa, em calda, enlatada, drenada");
        sparseArray.add("Atemóia, crua");
        sparseArray.add("Banana, da terra, crua ");
        sparseArray.add("Banana, doce em barra");
        sparseArray.add("Banana, figo, crua ");
        sparseArray.add("Banana, maçã, crua ");
        sparseArray.add("Banana, nanica, crua ");
        sparseArray.add("Banana, ouro, crua ");
        sparseArray.add("Banana, pacova, crua ");
        sparseArray.add("Banana, prata, crua");
        sparseArray.add("Cacau, cru ");
        sparseArray.add("Cajá-Manga, cru");
        sparseArray.add("Cajá, polpa, congelada ");
        sparseArray.add("Caju, cru");
        sparseArray.add("Caju, polpa, congelada ");
        sparseArray.add("Caju, suco concentrado, envasado ");
        sparseArray.add("Caqui, chocolate, cru");
        sparseArray.add("Carambola, crua");
        sparseArray.add("Ciriguela, crua");
        sparseArray.add("Cupuaçu, cru ");
        sparseArray.add("Cupuaçu, polpa, congelada");
        sparseArray.add("Figo, cru");
        sparseArray.add("Figo, enlatado, em calda ");
        sparseArray.add("Fruta-pão, crua");
        sparseArray.add("Goiaba, branca, com casca, crua");
        sparseArray.add("Goiaba, doce em pasta");
        sparseArray.add("Goiaba, doce, cascão ");
        sparseArray.add("Goiaba, vermelha, com casca, crua");
        sparseArray.add("Graviola, crua ");
        sparseArray.add("Graviola, polpa, congelada ");
        sparseArray.add("Jabuticaba, crua ");
        sparseArray.add("Jaca, crua ");
        sparseArray.add("Jambo, cru ");
        sparseArray.add("Jamelão, cru ");
        sparseArray.add("Kiwi, cru");
        sparseArray.add("Laranja, baía, crua");
        sparseArray.add("Laranja, baía, suco");
        sparseArray.add("Laranja, da terra, crua");
        sparseArray.add("Laranja, da terra, suco");
        sparseArray.add("Laranja, lima, crua");
        sparseArray.add("Laranja, lima, suco");
        sparseArray.add("Laranja, pêra, crua");
        sparseArray.add("Laranja, pêra, suco");
        sparseArray.add("Laranja, valência, crua");
        sparseArray.add("Laranja, valência, suco");
        sparseArray.add("Limão, cravo, suco ");
        sparseArray.add("Limão, galego, suco");
        sparseArray.add("Limão, tahiti, cru ");
        sparseArray.add("Maçã, Argentina, com casca, crua ");
        sparseArray.add("Maçã, Fuji, com casca, crua");
        sparseArray.add("Macaúba, crua");
        sparseArray.add(" Mamão, doce em calda, drenado ");
        sparseArray.add("Mamão, Formosa, cru");
        sparseArray.add("Mamão, Papaia, cru ");
        sparseArray.add(" Mamão verde, doce em calda, drenado ");
        sparseArray.add("Manga, Haden, crua ");
        sparseArray.add("Manga, Palmer, crua");
        sparseArray.add("Manga, polpa, congelada");
        sparseArray.add("Manga, Tommy Atkins, crua");
        sparseArray.add("Maracujá, cru");
        sparseArray.add("Maracujá, polpa, congelada ");
        sparseArray.add("Maracujá, suco concentrado, envasado ");
        sparseArray.add("Melancia, crua ");
        sparseArray.add("Melão, cru ");
        sparseArray.add("Mexerica, Murcote, crua");
        sparseArray.add("Mexerica, Rio, crua");
        sparseArray.add("Morango, cru ");
        sparseArray.add("Nêspera, crua");
        sparseArray.add("Pequi, cru ");
        sparseArray.add("Pêra, Park, crua ");
        sparseArray.add("Pêra, Williams, crua ");
        sparseArray.add("Pêssego, Aurora, cru ");
        sparseArray.add("Pêssego, enlatado, em calda");
        sparseArray.add("Pinha, crua");
        sparseArray.add("Pitanga, crua");
        sparseArray.add("Pitanga, polpa, congelada");
        sparseArray.add("Romã, crua ");
        sparseArray.add("Tamarindo, cru ");
        sparseArray.add("Tangerina, Poncã, crua ");
        sparseArray.add("Tangerina, Poncã, suco ");
        sparseArray.add("Tucumã, cru");
        sparseArray.add("Umbu, cru");
        sparseArray.add("Umbu, polpa, congelada ");
        sparseArray.add("Uva, Itália, crua");
        sparseArray.add("Uva, Rubi, crua");
        sparseArray.add("Uva, suco concentrado, envasado");
        sparseArray.add("Azeite, de dendê ");
        sparseArray.add("Azeite, de oliva, extra virgem ");
        sparseArray.add("Manteiga, com sal");
        sparseArray.add("Manteiga, sem sal");
        sparseArray.add("Margarina, com óleo hidrogenado, com sal (65% de lipídeos) ");
        sparseArray.add("Margarina, com óleo hidrogenado, sem sal (80% de lipídeos) ");
        sparseArray.add("Margarina, com óleo interesterificado, com sal (65%de lipídeos)");
        sparseArray.add("Margarina, com óleo interesterificado, sem sal (65% de lipídeos) ");
        sparseArray.add("Óleo, de babaçu");
        sparseArray.add("Óleo, de canola");
        sparseArray.add("Óleo, de girassol");
        sparseArray.add("Óleo, de milho ");
        sparseArray.add("Óleo, de pequi ");
        sparseArray.add("Óleo, de soja");
        sparseArray.add("Abadejo, filé, congelado, assado ");
        sparseArray.add("Abadejo, filé, congelado,cozido");
        sparseArray.add("Abadejo, filé, congelado, cru");
        sparseArray.add("Abadejo, filé, congelado, grelhado ");
        sparseArray.add("Atum, conserva em óleo ");
        sparseArray.add("Atum, fresco, cru");
        sparseArray.add("Bacalhau, salgado, cru ");
        sparseArray.add("Bacalhau, salgado, refogado");
        sparseArray.add("Cação, posta, com farinha de trigo, frita");
        sparseArray.add("Cação, posta, cozida ");
        sparseArray.add("Cação, posta, crua ");
        sparseArray.add("Camarão, Rio Grande, grande, cozido");
        sparseArray.add("Camarão, Rio Grande, grande, cru ");
        sparseArray.add("Camarão, Sete Barbas, sem cabeça, com casca, frito ");
        sparseArray.add("Caranguejo, cozido ");
        sparseArray.add("Corimba, cru ");
        sparseArray.add("Corimbatá, assado");
        sparseArray.add("Corimbatá, cozido");
        sparseArray.add("Corvina de água doce, crua ");
        sparseArray.add("Corvina do mar, crua ");
        sparseArray.add("Corvina grande, assada ");
        sparseArray.add("Corvina grande, cozida ");
        sparseArray.add("Dourada de água doce, fresca ");
        sparseArray.add("Lambari, congelado, cru");
        sparseArray.add("Lambari, congelado, frito");
        sparseArray.add("Lambari, fresco, cru ");
        sparseArray.add("Manjuba, com farinha de trigo, frita ");
        sparseArray.add("Manjuba, frita ");
        sparseArray.add("Merluza, filé, assado");
        sparseArray.add("Merluza, filé, cru ");
        sparseArray.add("Merluza, filé, frito ");
        sparseArray.add("Pescada, branca, crua");
        sparseArray.add("Pescada, branca, frita ");
        sparseArray.add("Pescada, filé, com farinha de trigo, frito ");
        sparseArray.add("Pescada, filé, cru ");
        sparseArray.add("Pescada, filé, frito ");
        sparseArray.add("Pescada, filé, molho escabeche ");
        sparseArray.add("Pescadinha, crua ");
        sparseArray.add("Pintado, assado");
        sparseArray.add("Pintado, cru ");
        sparseArray.add("Pintado, grelhado");
        sparseArray.add("Porquinho, cru ");
        sparseArray.add("Salmão, filé, com pele, fresco,grelhado");
        sparseArray.add("Salmão, sem pele, fresco, cru");
        sparseArray.add("Salmão, sem pele, fresco, grelhado ");
        sparseArray.add("Sardinha, assada ");
        sparseArray.add("Sardinha, conserva em óleo ");
        sparseArray.add("Sardinha, frita");
        sparseArray.add("Sardinha, inteira, crua");
        sparseArray.add("Tucunaré, filé, congelado, cru ");
        sparseArray.add("Apresuntado");
        sparseArray.add("Caldo de carne, tablete");
        sparseArray.add("Caldo de galinha, tablete");
        sparseArray.add("Carne, bovina, acém, moído, cozido ");
        sparseArray.add("Carne, bovina, acém, moído, cru");
        sparseArray.add("Carne, bovina, acém, sem gordura, cozido ");
        sparseArray.add("Carne, bovina, acém, sem gordura, cru");
        sparseArray.add("Carne, bovina, almôndegas, cruas ");
        sparseArray.add("Carne, bovina, almôndegas, fritas");
        sparseArray.add("Carne, bovina, bucho, cozido ");
        sparseArray.add("Carne, bovina, bucho, cru");
        sparseArray.add("Carne, bovina, capa de contra-filé, com gordura, crua");
        sparseArray.add("Carne, bovina, capa de contra-filé, com gordura, grelhada");
        sparseArray.add("Carne, bovina, capa de contra-filé, sem gordura, crua");
        sparseArray.add("Carne, bovina, capa de contra-filé, sem gordura, grelhada");
        sparseArray.add("Carne, bovina, charque, cozido ");
        sparseArray.add("Carne, bovina, charque, cru");
        sparseArray.add("Carne, bovina, contra-filé, à milanesa ");
        sparseArray.add("Carne, bovina, contra-filé de costela, cru ");
        sparseArray.add("Carne, bovina, contra-filé de costela, grelhado");
        sparseArray.add("Carne, bovina, contra-filé, com gordura, cru ");
        sparseArray.add("Carne, bovina, contra-filé, com gordura, grelhado");
        sparseArray.add("Carne, bovina, contra-filé, sem gordura, cru ");
        sparseArray.add("Carne, bovina, contra-filé, sem gordura, grelhado");
        sparseArray.add("Carne, bovina, costela, assada ");
        sparseArray.add("Carne, bovina, costela, crua ");
        sparseArray.add("Carne, bovina, coxão duro, sem gordura, cozido ");
        sparseArray.add("Carne, bovina, coxão duro, sem gordura, cru");
        sparseArray.add("Carne, bovina, coxão mole, sem gordura, cozido ");
        sparseArray.add("Carne, bovina, coxão mole, sem gordura, cru");
        sparseArray.add("Carne, bovina, cupim, assado ");
        sparseArray.add("Carne, bovina, cupim, cru");
        sparseArray.add("Carne, bovina, fígado, cru ");
        sparseArray.add("Carne, bovina, fígado, grelhado");
        sparseArray.add("Carne, bovina, filé mingnon, sem gordura, cru");
        sparseArray.add("Carne, bovina, filé mingnon, sem gordura, grelhado ");
        sparseArray.add("Carne, bovina, flanco, sem gordura, cozido ");
        sparseArray.add("Carne, bovina, flanco, sem gordura, cru");
        sparseArray.add("Carne, bovina, fraldinha, com gordura, cozida");
        sparseArray.add("Carne, bovina, fraldinha, com gordura, crua");
        sparseArray.add("Carne, bovina, lagarto, cozido ");
        sparseArray.add("Carne, bovina, lagarto, cru");
        sparseArray.add("Carne, bovina, língua, cozida");
        sparseArray.add("Carne, bovina, língua, crua");
        sparseArray.add("Carne, bovina, maminha, crua ");
        sparseArray.add("Carne, bovina, maminha, grelhada ");
        sparseArray.add("Carne, bovina, miolo de alcatra, sem gordura, cru");
        sparseArray.add("Carne, bovina, miolo de alcatra, sem gordura, grelhado ");
        sparseArray.add("Carne, bovina, músculo, sem gordura, cozido");
        sparseArray.add("Carne, bovina, músculo, sem gordura, cru ");
        sparseArray.add("Carne, bovina, paleta, com gordura, crua ");
        sparseArray.add("Carne, bovina, paleta, sem gordura, cozida ");
        sparseArray.add("Carne, bovina, paleta, sem gordura, crua ");
        sparseArray.add("Carne, bovina, patinho, sem gordura, cru ");
        sparseArray.add("Carne, bovina, patinho, sem gordura, grelhado");
        sparseArray.add("Carne, bovina, peito, sem gordura, cozido");
        sparseArray.add("Carne, bovina, peito, sem gordura, cru ");
        sparseArray.add("Carne, bovina, picanha, com gordura, crua");
        sparseArray.add("Carne, bovina, picanha, com gordura, grelhada");
        sparseArray.add("Carne, bovina, picanha, sem gordura, crua");
        sparseArray.add("Carne, bovina, picanha, sem gordura, grelhada");
        sparseArray.add("Carne, bovina, seca, cozida");
        sparseArray.add("Carne, bovina, seca, crua");
        sparseArray.add("Coxinha de frango, frita ");
        sparseArray.add("Croquete, de carne, cru");
        sparseArray.add("Croquete, de carne, frito");
        sparseArray.add("Empada de frango, pré-cozida, assada ");
        sparseArray.add("Empada, de frango, pré-cozida");
        sparseArray.add("Frango, asa, com pele, crua");
        sparseArray.add("Frango, caipira, inteiro, com pele, cozido ");
        sparseArray.add("Frango, caipira, inteiro, sem pele, cozido ");
        sparseArray.add("Frango, coração, cru ");
        sparseArray.add("Frango, coração, grelhado");
        sparseArray.add("Frango, coxa, com pele, assada ");
        sparseArray.add("Frango, coxa, com pele, crua ");
        sparseArray.add("Frango, coxa, sem pele, cozida ");
        sparseArray.add("Frango, coxa, sem pele, crua ");
        sparseArray.add("Frango, fígado, cru");
        sparseArray.add("Frango, filé, à milanesa ");
        sparseArray.add("Frango, inteiro, com pele, cru ");
        sparseArray.add("Frango, inteiro, sem pele, assado");
        sparseArray.add("Frango, inteiro, sem pele, cozido");
        sparseArray.add("Frango, inteiro, sem pele, cru ");
        sparseArray.add("Frango, peito, com pele, assado");
        sparseArray.add("Frango, peito, com pele, cru ");
        sparseArray.add("Frango, peito, sem pele, cozido");
        sparseArray.add("Frango, peito, sem pele, cru ");
        sparseArray.add("Frango, peito, sem pele, grelhado");
        sparseArray.add("Frango, sobrecoxa, com pele, assada");
        sparseArray.add("Frango, sobrecoxa, com pele, crua");
        sparseArray.add("Frango, sobrecoxa, sem pele, assada");
        sparseArray.add("Frango, sobrecoxa, sem pele, crua");
        sparseArray.add("Hambúrguer, bovino, cru");
        sparseArray.add("Hambúrguer, bovino, frito");
        sparseArray.add("Hambúrguer, bovino, grelhado ");
        sparseArray.add("Lingüiça, frango, crua ");
        sparseArray.add("Lingüiça, frango, frita");
        sparseArray.add("Lingüiça, frango, grelhada ");
        sparseArray.add("Lingüiça, porco, crua");
        sparseArray.add("Lingüiça, porco, frita ");
        sparseArray.add("Lingüiça, porco, grelhada");
        sparseArray.add("Mortadela");
        sparseArray.add("Peru, congelado, assado");
        sparseArray.add("Peru, congelado, cru ");
        sparseArray.add("Porco, bisteca, crua ");
        sparseArray.add("Porco, bisteca, frita");
        sparseArray.add("Porco, bisteca, grelhada ");
        sparseArray.add("Porco, costela, assada ");
        sparseArray.add("Porco, costela, crua ");
        sparseArray.add("Porco, lombo, assado ");
        sparseArray.add("Porco, lombo, cru");
        sparseArray.add("Porco, orelha, salgada, crua ");
        sparseArray.add("Porco, pernil, assado");
        sparseArray.add("Porco, pernil, cru ");
        sparseArray.add("Porco, rabo, salgado, cru");
        sparseArray.add("Presunto, com capa de gordura");
        sparseArray.add("Presunto, sem capa de gordura");
        sparseArray.add("Quibe, assado");
        sparseArray.add("Quibe, cru ");
        sparseArray.add("Quibe, frito ");
        sparseArray.add("Salame ");
        sparseArray.add("Toucinho, cru");
        sparseArray.add("Toucinho, frito");
        sparseArray.add("Bebida láctea, pêssego ");
        sparseArray.add("Creme de Leite ");
        sparseArray.add("Iogurte, natural ");
        sparseArray.add("Iogurte, natural, desnatado");
        sparseArray.add("Iogurte, sabor abacaxi ");
        sparseArray.add("Iogurte, sabor morango ");
        sparseArray.add("Iogurte, sabor pêssego ");
        sparseArray.add("Leite, condensado");
        sparseArray.add("Leite, de cabra");
        sparseArray.add("Leite, de vaca, achocolatado ");
        sparseArray.add("Leite, de vaca, desnatado, pó");
        sparseArray.add("Leite, de vaca, desnatado, UHT ");
        sparseArray.add("Leite, de vaca, integral ");
        sparseArray.add("Leite, de vaca, integral, pó ");
        sparseArray.add("Leite, fermentado");
        sparseArray.add("Queijo, minas, frescal ");
        sparseArray.add("Queijo, minas, meia cura ");
        sparseArray.add("Queijo, mozarela ");
        sparseArray.add("Queijo, parmesão ");
        sparseArray.add("Queijo, pasteurizado ");
        sparseArray.add("Queijo, petit suisse, morango");
        sparseArray.add("Queijo, prato");
        sparseArray.add("Queijo, requeijão, cremoso ");
        sparseArray.add("Queijo, ricota ");
        sparseArray.add("Bebida isotônica, sabores variados ");
        sparseArray.add("Café, infusão 10%");
        sparseArray.add("Cana, aguardente 1 ");
        sparseArray.add("Cana, caldo de ");
        sparseArray.add("Cerveja, pilsen 2");
        sparseArray.add("Chá, erva-doce, infusão 5% ");
        sparseArray.add("Chá, mate, infusão 5%");
        sparseArray.add("Chá, preto, infusão 5% ");
        sparseArray.add("Coco, água de");
        sparseArray.add("Refrigerante, tipo água tônica ");
        sparseArray.add("Refrigerante, tipo cola");
        sparseArray.add("Refrigerante, tipo guaraná ");
        sparseArray.add("Refrigerante, tipo laranja ");
        sparseArray.add("Refrigerante, tipo limão ");
        sparseArray.add("Omelete, de queijo ");
        sparseArray.add("Ovo, de codorna, inteiro, cru");
        sparseArray.add("Ovo, de galinha, clara, cozida/10minutos ");
        sparseArray.add("Ovo, de galinha, gema, cozida/10minutos");
        sparseArray.add("Ovo, de galinha, inteiro, cozido/10minutos ");
        sparseArray.add("Ovo, de galinha, inteiro, cru");
        sparseArray.add("Ovo, de galinha, inteiro, frito");
        sparseArray.add("Achocolatado, pó ");
        sparseArray.add("Açúcar, cristal");
        sparseArray.add("Açúcar, mascavo");
        sparseArray.add("Açúcar, refinado ");
        sparseArray.add("Chocolate, ao leite");
        sparseArray.add("Chocolate, ao leite, com castanha do Pará");
        sparseArray.add("Chocolate, ao leite, dietético ");
        sparseArray.add("Chocolate, meio amargo ");
        sparseArray.add("Cocada branca");
        sparseArray.add("Doce, de abóbora, cremoso");
        sparseArray.add("Doce, de leite, cremoso");
        sparseArray.add("Geléia, mocotó, natural");
        sparseArray.add("Glicose de milho ");
        sparseArray.add("Maria mole ");
        sparseArray.add("Maria mole, coco queimado");
        sparseArray.add("Marmelada");
        sparseArray.add("Mel, de abelha ");
        sparseArray.add("Melado ");
        sparseArray.add("Quindim");
        sparseArray.add("Rapadura ");
        sparseArray.add("Café, pó, torrado");
        sparseArray.add("Capuccino, pó");
        sparseArray.add("Fermento em pó, químico");
        sparseArray.add("Fermento, biológico, levedura, tablete ");
        sparseArray.add("Gelatina, sabores variados, pó ");
        sparseArray.add("Sal, dietético ");
        sparseArray.add("Sal, grosso");
        sparseArray.add("Shoyu");
        sparseArray.add("Tempero a base de sal");
        sparseArray.add("Azeitona, preta, conserva");
        sparseArray.add("Azeitona, verde, conserva");
        sparseArray.add("Chantilly, spray, com gordura vegetal");
        sparseArray.add("Leite, de coco ");
        sparseArray.add("Maionese, tradicional com ovos ");
        sparseArray.add("Acarajé");
        sparseArray.add("Arroz carreteiro ");
        sparseArray.add("Baião de dois, arroz e feijão-de-corda ");
        sparseArray.add("Barreado ");
        sparseArray.add("Bife à cavalo, com contra filé ");
        sparseArray.add("Bolinho de arroz ");
        sparseArray.add("Camarão à baiana ");
        sparseArray.add("Charuto, de repolho");
        sparseArray.add("Cuscuz, de milho, cozido com sal ");
        sparseArray.add("Cuscuz, paulista ");
        sparseArray.add("Cuxá, molho");
        sparseArray.add("Dobradinha ");
        sparseArray.add("Estrogonofe de carne ");
        sparseArray.add("Estrogonofe de frango");
        sparseArray.add("Feijão tropeiro mineiro");
        sparseArray.add("Feijoada ");
        sparseArray.add("Frango, com açafrão");
        sparseArray.add("Macarrão, molho bolognesa");
        sparseArray.add("Maniçoba ");
        sparseArray.add("Quibebe");
        sparseArray.add("Salada, de legumes, com maionese ");
        sparseArray.add("Salada, de legumes, cozida no vapor");
        sparseArray.add("Salpicão, de frango");
        sparseArray.add("Sarapatel");
        sparseArray.add("Tabule ");
        sparseArray.add("Tacacá ");
        sparseArray.add("Tapioca, com manteiga");
        sparseArray.add("Tucupi, com pimenta-de-cheiro");
        sparseArray.add("Vaca atolada ");
        sparseArray.add("Vatapá ");
        sparseArray.add("Virado à paulista");
        sparseArray.add("Yakisoba ");
        sparseArray.add("Amendoim, grão, cru");
        sparseArray.add("Amendoim, torrado, salgado ");
        sparseArray.add("Ervilha, em vagem");
        sparseArray.add("Ervilha, enlatada, drenada ");
        sparseArray.add("Feijão, carioca, cozido");
        sparseArray.add("Feijão, carioca, cru ");
        sparseArray.add("Feijão, fradinho, cozido ");
        sparseArray.add("Feijão, fradinho, cru");
        sparseArray.add("Feijão, jalo, cozido ");
        sparseArray.add("Feijão, jalo, cru");
        sparseArray.add("Feijão, preto, cozido");
        sparseArray.add("Feijão, preto, cru ");
        sparseArray.add("Feijão, rajado, cozido ");
        sparseArray.add("Feijão, rajado, cru");
        sparseArray.add("Feijão, rosinha, cozido");
        sparseArray.add("Feijão, rosinha, cru ");
        sparseArray.add("Feijão, roxo, cozido ");
        sparseArray.add("Feijão, roxo, cru");
        sparseArray.add("Grão-de-bico, cru");
        sparseArray.add("Guandu, cru");
        sparseArray.add("Lentilha, cozida ");
        sparseArray.add("Lentilha, crua ");
        sparseArray.add("Paçoca, amendoim ");
        sparseArray.add("Pé-de-moleque, amendoim");
        sparseArray.add("Soja, farinha");
        sparseArray.add("Soja, extrato solúvel, natural, fluido ");
        sparseArray.add("Soja, extrato solúvel, pó");
        sparseArray.add("Soja, queijo (tofu)");
        sparseArray.add("Tremoço, cru ");
        sparseArray.add("Tremoço, em conserva ");
        sparseArray.add("Amêndoa, torrada, salgada");
        sparseArray.add("Castanha-de-caju, torrada, salgada ");
        sparseArray.add("Castanha-do-Brasil, crua ");
        sparseArray.add("Coco, cru");
        sparseArray.add("Coco,verde, cru");
        sparseArray.add("Farinha, de mesocarpo de babaçu, crua");
        sparseArray.add("Gergelim, semente");
        sparseArray.add("Linhaça, semente ");
        sparseArray.add("Pinhão, cozido ");
        sparseArray.add("Pupunha, cozida");
        sparseArray.add("Noz, crua");
        return sparseArray;
    }
}
