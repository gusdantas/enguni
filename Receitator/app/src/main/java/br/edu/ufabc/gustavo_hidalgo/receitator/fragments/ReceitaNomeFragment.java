package br.edu.ufabc.gustavo_hidalgo.receitator.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import br.edu.ufabc.gustavo_hidalgo.receitator.R;
import br.edu.ufabc.gustavo_hidalgo.receitator.constants.Constantes;
import br.edu.ufabc.gustavo_hidalgo.receitator.interfaces.IMainActivity;
import br.edu.ufabc.gustavo_hidalgo.receitator.models.Receita;

public class ReceitaNomeFragment extends Fragment implements View.OnClickListener {
    public Button mNomeRecPronto, mVoltar;
    public EditText mNomeRec, mServeQts;
    static Receita mReceita;
    private IMainActivity mIMainActivity;
    public static final String TAG = "[Rectt]NRNFr";

    public ReceitaNomeFragment() {
        // Required empty public constructor
    }

    public static ReceitaNomeFragment newInstance(Receita receita) {
        ReceitaNomeFragment fragment = new ReceitaNomeFragment();
        mReceita = receita;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "create");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "view");
        View view = inflater.inflate(R.layout.fragment_receita_nome, container, false);
        mNomeRec = (EditText) view.findViewById(R.id.nomeReceitaEditText);
        mServeQts = (EditText) view.findViewById(R.id.serveQtEditText);
        mNomeRecPronto = (Button) view.findViewById(R.id.ingRecOKBt);
        mVoltar = (Button) view.findViewById(R.id.voltarButton);
        mNomeRecPronto.setOnClickListener(this);
        mVoltar.setOnClickListener(this);
        // Inflate the layout for this fragment
        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ingRecOKBt:
                Log.d(TAG,"oC nomeRecOKBt");
                mReceita.setNome(mNomeRec.getText().toString());
                mReceita.setServeQts(Integer.valueOf(mServeQts.getText().toString()));
                changeFragment(Constantes.RECEITA_ING, mReceita);
                break;
            case R.id.voltarButton:
                Log.d(TAG,"oC voltar");
                changeFragment(Constantes.TELA_INICIAL, null);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mIMainActivity = (IMainActivity) activity;
    }

    public void changeFragment(int data, Receita receita) {
        mIMainActivity.changeFragment(data, receita);
    }
}
